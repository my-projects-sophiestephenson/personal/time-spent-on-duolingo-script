##
##  get hours on duolingo using imap-tools
##  sophie stephenson
##  august 2020
##

import yaml
from imap_tools import MailBox, AND
from html.parser import HTMLParser
from html.entities import name2codepoint
from datetime import datetime

# get credentials 
with open('emailcreds.yaml') as f:
    creds = yaml.load(f, Loader=yaml.FullLoader)

# set up mailbox
setup_mailbox = MailBox('imap.gmail.com')
print("Logging in ...")
setup_mailbox.login(creds['email'], creds['pass'])
folders = [folder['name'] for folder in setup_mailbox.folder.list() if "[Gmail]" not in folder['name']]
setup_mailbox.logout()

# get html of all duolingo weekly progress report messages in every folder
msgs = []
for folder in folders:
    if folder != "":
        print("Checking", folder, "...")
        mailbox = MailBox('imap.gmail.com')
        mailbox.login(creds['email'], creds['pass'], initial_folder=folder)
        fetch = mailbox.fetch(AND(from_="hello@duolingo.com", subject="Your weekly progress report"))
        msgs += [msg.html for msg in fetch]
mailbox.logout()


# set up html parser
class MyHTMLParser(HTMLParser):
    html_data = []
    
    def handle_data(self, data):
        stripped = data.strip()
        if stripped != "":
            self.html_data.append(stripped)

    def reset_html_data(self):
        self.html_data = []
        
parser = MyHTMLParser()

# calculate the total minutes spent by reading the messages
total_minutes = 0
for msg in msgs:
    # feed the message through the parser
    parser.feed(msg)

    # find the string representing the time spent
    # to do so, we find the position of the Time Spent label element, and the actual time spent
    #   information should be three spots over from the label
    if "Time Spent" not in parser.html_data: continue
    time_spent_label_index = parser.html_data.index("Time Spent")
    time_spent_string = parser.html_data[time_spent_label_index + 3]
    time_spent = datetime.strptime(time_spent_string, '%Hh %Mm')
    total_minutes += (60 * time_spent.hour) + time_spent.minute
    
    parser.reset_html_data()

# use minutes spent to display the total time spent on Duolingo
total_hours_spent = int(total_minutes / 60)
leftover_minutes = total_minutes - total_hours_spent * 60
print("\nTime spent on Duolingo:", total_hours_spent, "hours,", leftover_minutes, "minutes.")

    
