# How much time have you spent on Duolingo?

## Background
Since November 2019, I've been learning Scottish Gaelic on [Duolingo](https://www.duolingo.com). I was curious about how much time I've spent on the app, but Duolingo doesn't tell users how much time they've spent on the app; I wrote this script to figure it out for myself. Apparently (as of August 2, 2020), I've spent over 16 hours, 37 minutes on Duolingo!

This script gets an approximate calculation of time spent on the app by accessing the weekly progress report emails a user has received from Duolingo. Each of these emails contains the amount of time spent on the app during the past week, so the script reads these progress reports, gets the time spent each week, and finds the total time spent using these values.

## Setup
1. Download the necessary tools:
    - Download [Python](https://www.python.org/download/releases/3.0/) if it isn't already on your device.
    - Download [imap-tools](https://pypi.org/project/imap-tools/#guide) by running `pip install imap-tools` in terminal.
2. Download this repository anywhere on your device.
3. Edit `emailcreds.yaml` to contain the email* you want to search and the corresponding password.**
4. In terminal, navigate to the project directory and run `python get-hours-on-duolingo.py`.

**Disclaimer: I'm pretty sure this has to be a Gmail account in order to work with what I've written. If you are not using Gmail, you may need to make some tweaks to make it work with your email - for instance, you may have to change "imap.gmail.com" to "imap.email.com" on line 18.*

***If you*  **are** *using a Gmail account like me, you'll need to first create an app password in order to access your email via `imap_tools`. To generate this password, go to your Google account, select Security, and under "Signing in to Google", select App Passwords. There you should be able to generate and copy your new app password. See the Google Support [article](https://support.google.com/accounts/answer/185833?hl=en) on this topic for more information.*

## Troubleshooting

If the script doesn't seem to be finding all of your progress emails, try moving them all to your inbox. I ran into the issue that my Archive folder did not appear in my folders list, so any archived emails (many) were not being included.
